# DPL - Dismissive Public License

This is intended to be the free'est license applicable to software, hardware
and anything else, without inadvertently allowing loopholes for inadvertent
effects.
It is intended to be as minimal as feasible, logically accurate, and to a
reasonable degree "legaly correct".

```
[What is this text/license?]
This text expresses the author(s) wishes and intends regarding rights
pertaining to the accompaning work?

[Definitions?]
"The work" / "this work"?

[What does it govern?]
The accompanying intellectual work or product.
Software source files, other source files, music, text, or anything at all that
is provided/distributed along with this license.
Except for "contrib" sources that are already owned/governed by other people or
other licences.

[Whom does it grant to?]
Any entity in the universe that is capable of interacting with the work.

[What does it allow?]
Any manners of usage.
Usage for any intents or purposes.
Including distribution, modification, anything.

[What does it disallow?]
All warranty is disclaimed.
Can't hold liable.
The author(s) are not accountable for anything.
No endorsements.
Nothing involving the author(s) except attribution?

[What does it require?]
Nothing except obediance to this license.
Not even a copyright notice.
Not even attribution (although that could be nice).
Especially not copyleft.

[Misc]
Any implicit copyright/ip laws in any country is entirely dismissed and
substituted with the wishes/intends expressed in this license.
No ownership is ever to be held over the work, or rather it is in perpetuity
owned collectively and equally for all entities that this license grants to.
```
